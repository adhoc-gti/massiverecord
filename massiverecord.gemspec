Gem::Specification.new do |s|
    s.name        = 'massiverecord'
    s.version     = '0.0.0'
    s.licenses    = ['BSD-3-Clause']
    s.summary     = 'Fast mass interactions atop ActiveRecord'
    s.description = <<-DESC
      Leverage Active Record introspection to perform fast mass interactions
      skipping record-level model instantiation.
    DESC
    s.authors     = ['ADHOC-GTI']
    s.email       = 'foss@adhoc-gti.com'
    s.files       = Dir['lib/**/*.rb'] + ['LICENSE']
    s.homepage    = 'https://github.com/adhoc-gti/massiverecord'
  end