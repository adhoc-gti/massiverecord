# MassiveRecord

Leverage Active Record introspection to perform fast mass interactions
skipping record-level model instantiation.
